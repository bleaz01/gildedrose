class Item {
  constructor(name, sellIn, quality) {
    this.name = name;
    this.sellIn = sellIn;
    this.quality = quality;
  }
}

class Shop {
  constructor(items = []) {
    this.items = items;
  }



  updateQuality() {
    // const { items } = this is for more visibility in my code 
    const { items } = this;

    //I have created functions inside the function "updateQuality" to better manage the product and to have an easier debugging

    const handleBackstage = () => {

      if (items[i].sellIn >= 0 && items[i].quality < 50) {
        if (items[i].sellIn < 5 && items[i].quality < 47) items[i].quality += 3;
        else if (items[i].sellIn < 10 && items[i].quality < 48) items[i].quality += 2;
        else if (items[i].sellIn > 1) items[i].quality += 1
      }
      else items[i].quality = 0
    }

    const handelChess = () => {

      if (items[i].quality < 50) {
        items[i].quality++;
      }
      if (items[i].quality < 0) items[i].quality = 0
    }

    const other = () => {

      if (items[i].quality > 0) {
        if (items[i].sellIn > 0) {
          items[i].quality -= 1
        } else {
          if (items[i].quality = 1) items[i].quality--
          else items[i].quality -= 2;
        }
      } else items[i].quality = 0

    };


    for (var i = 0; i < items.length; i++) {
      let name = items[i].name

      if (name !== 'Sulfuras, Hand of Ragnaros') {
        items[i].sellIn = items[i].sellIn - 1;
        if (items[i].quality < 50) {
          if (name.match('Conjured')) {
            if (items[i].quality > 0) items[i].quality -= 2
            else items[i].quality = 0
          } else if (name.match('Aged Brie') || name.match('Backstage passes to a TAFKAL80ETC concert')) {
            name === 'Aged Brie' ? handelChess() : handleBackstage()
          } else {
            other()
          }
        } else items[i].quality = 50

      }
      else {
        if (items[i].sellIn < 0) items[i].sellIn = 0
      }
      // I have used the match function to have a condition that can react to all the other products 
      // I have grab the product name and then give it the function assigned to it

    }
    return items;
  }

}

module.exports = {
  Item,
  Shop,
}