
# GildedRoseAPP



## Compile and run this code

  *Open your terminal*
1. Clone the repository
bash
git clone https://gitlab.com/bleaz01/gildedrose.git

2. Enter into the project
bash
cd js-mocha


3. Install the modules needed for making the project run
bash
npm i --save


4. Start the project
bash
npm test


author: Riguelle Jeason