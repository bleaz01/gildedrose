
const { Shop, Item } = require("../src/gilded_rose");

const items = [

  // I have changed the  data for the test

  new Item("+5 Dexterity Vest", 10, 50),
  new Item("Aged Brie", 2, 51),
  new Item("Aged Brie", 2, 0),
  new Item("Elixir of the Mongoose", 5, 4),
  new Item("Elixir of the Mongoose", 5, 0),
  new Item("Sulfuras, Hand of Ragnaros", 0, 80),
  new Item("Sulfuras, Hand of Ragnaros", -1, 80),
  new Item("Backstage passes to a TAFKAL80ETC concert", 11, 0),
  new Item("Backstage passes to a TAFKAL80ETC concert", 5, 30),
  new Item("Backstage passes to a TAFKAL80ETC concert", 0, 23),
  new Item("Backstage passes to a TAFKAL80ETC concert", 8, 24),
  new Item("Backstage passes to a TAFKAL80ETC concert", 14, 34),


  // new Item("+5 Dexterity Vest", 10, 20),
  // new Item("Aged Brie", 2, 0),
  // new Item("Elixir of the Mongoose", 2, 7),
  // new Item("Sulfuras, Hand of Ragnaros", 0, 80),
  // new Item("Sulfuras, Hand of Ragnaros", -1, 80),
  // new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20),
  // new Item("Backstage passes to a TAFKAL80ETC concert", 10, 49),
  // new Item("Backstage passes to a TAFKAL80ETC concert", 5, 49),

  // This Conjured item does not work properly yet
  new Item("Conjured Mana Cake", 3, 6),
];

const days = Number(process.argv[2]) || 2
  ;
const gildedRose = new Shop(items);

console.log("OMGHAI!");
for (let day = 0; day < days; day++) {
  console.log(`\n-------- day ${day} --------`);
  console.log("name, sellIn, quality");
  items.forEach(item => console.log(`${item.name}, ${item.sellIn}, ${item.quality}`));
  gildedRose.updateQuality();
}
