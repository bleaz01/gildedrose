var { expect } = require('chai');
var { Shop, Item } = require('../src/gilded_rose.js');

const items = [
  new Item("+5 Dexterity Vest", 10, 50),
  new Item("Aged Brie", 2, 50),
  new Item("Aged Brie", 2, 0),
  new Item("Elixir of the Mongoose", 5, 4),
  new Item("Elixir of the Mongoose", 5, 0),
  new Item("Sulfuras, Hand of Ragnaros", 0, 80),
  new Item("Sulfuras, Hand of Ragnaros", -1, 80),
  new Item("Backstage passes to a TAFKAL80ETC concert", 11, 50),
  new Item("Backstage passes to a TAFKAL80ETC concert", 5, 30),
  new Item("Backstage passes to a TAFKAL80ETC concert", 0, 23),
  new Item("Backstage passes to a TAFKAL80ETC concert", 8, 24),
  new Item("Backstage passes to a TAFKAL80ETC concert", 14, 34),


];
const gildedRose = new Shop(items);

describe("Gilded Rose", function () {

  it("produc (Backstage and Aged Brie) quality + 1", function () {
    const items = gildedRose.updateQuality();
    expect(items[2].quality).to.equal(1);
    expect(items[11].quality).to.equal(35);
  });

  it("produc quality is not < 0", function () {
    const items = gildedRose.updateQuality();
    expect(items[0].quality).to.not.equal(-1);
    expect(items[4].quality).to.equal(0);
  });

  it("produc != Legende is not quality > 50", function () {
    const items = gildedRose.updateQuality();
    expect(items[1].quality).to.equal(50);
    expect(items[7].quality).to.equal(50);

  });

  it("Legend produc not sale and not quality -1", function () {
    const items = gildedRose.updateQuality();
    expect(items[5].quality).to.not.equal(79);
    expect(items[5].sellIn).to.equal(0);

  });

  //for this test I have change the methode because of the data quality and sell In is not assigne correcly 

  it("Backstage quality + 2 if sell In < 11", function () {
    const gildedRose = new Shop([new Item('Backstage passes to a TAFKAL80ETC concert', 8, 23)])
    const items = gildedRose.updateQuality();
    expect(items[0].quality).to.equal(25)

  });
  it("Backstage quality + 3 if sell In < 6", function () {
    const gildedRose = new Shop([new Item('Backstage passes to a TAFKAL80ETC concert', 3, 21)])
    const items = gildedRose.updateQuality();
    expect(items[0].quality).to.equal(24);

  });
  it("Backstage sellIn == 0 quality == 0", function () {
    const items = gildedRose.updateQuality();
    expect(items[9].quality).to.equal(0);

  });
  it("conjured * 2 then noraml produc", function () {
    const gildedRose = new Shop([new Item("Conjured Mana Cake", 3, 6)])
    const items = gildedRose.updateQuality();
    expect(items[0].quality).to.equal(4);

  });

});
